import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import DrumContainer from './DrumComponent/drumcontainer';

ReactDOM.render(<DrumContainer />, document.getElementById('root'));
registerServiceWorker();
