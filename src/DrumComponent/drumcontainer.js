
import React from 'react';
import DrumDisplay from './drumdisplay';
import DrumPad from './drumpad'
import Switch from './switch'
import Slider from './slider'
import data from './keydata.json'
import './drumcontainer.css'

class DrumContainer extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            display: "Rock On!!!",
            power : true,
            bank : true,
            volume : 50 
        }
        this.setDisplayText = this.setDisplayText.bind(this);
        this.togglePower = this.togglePower.bind(this);
        this.toggleBank = this.toggleBank.bind(this);
        this.setVolume = this.setVolume.bind(this);
    }

    setDisplayText(text){
        this.setState(Object.assign({}, this.state, {display: text}));
    };

    togglePower(){
        this.setState(Object.assign({}, this.state, {display : !this.state.power? "Rock On!!" : "Sound Muted!!" ,power: !this.state.power}));
    }

    toggleBank(){
        this.setState(Object.assign({}, this.state, {display : !this.state.bank? "Smooth Piano Kit" : "Heater Kit" ,bank: !this.state.bank}));
    }

    setVolume(volume){
        this.setState(Object.assign({}, this.state, {volume: volume}));
    }


    
    render(){
        return(
            <div id="drum-machine">
                <div className="keys">
                    {data.map((a,b) =>{
                        return <DrumPad key={b} setDisplayText={this.setDisplayText} power={this.state.power} bank={this.state.bank} volume={this.state.volume} data={a}></DrumPad>
                    })}
                </div>
                <div className="controls">
                    <Switch name={"POWER"} setDisplayText={this.setDisplayText} toggle={this.togglePower} power={this.state.power}></Switch>
                    <DrumDisplay display={this.state.display}></DrumDisplay>
                    <Slider setValue={this.setVolume} volume={this.state.volume} power={this.state.power}></Slider>
                    <Switch name={"BANK"} setDisplayText={this.setDisplayText} toggle={this.toggleBank} ></Switch>
                </div> 
            </div>
        )
    }
}

export default DrumContainer;