import React from 'react'
import './drumpad.css'

class DrumPad extends React.Component{
    constructor(props){
        super(props)
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick () {
        this.props.setDisplayText(this.props.bank ? this.props.data.notename1 : this.props.data.notename2);
        const element = document.getElementById(this.props.data.keyname);
        element.pause();element.load();element.play();
    }

    componentWillReceiveProps(previous,actual){
        const element = document.getElementById(this.props.data.keyname);
        element.volume = this.props.volume*.01;
    }

    render(){
        return (
            <div className="drumPad">
                <button onClick={this.handleClick} disabled={!this.props.power} className="drumpad-text">{this.props.data.keyname}</button>
                <audio id={this.props.data.keyname} volume={this.props.volume*.01}> 
                    <source src={this.props.bank ? this.props.data.audio1 : this.props.data.audio2} type="audio/ogg"></source>
                    <source src={this.props.bank ? this.props.data.audio1 : this.props.data.audio2} type="audio/mpeg"></source>
                    Your browser does not support the audio element.
                </audio>
            </div>
        );
    }
}

export default DrumPad;