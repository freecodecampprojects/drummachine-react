import React from 'react'
import './drumdisplay.css'

class DrumDisplay extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        return (
            <div className="display">
                <p>{this.props.display}</p>
            </div>
        );
    }
}

export default DrumDisplay;