import React from 'react'
import './switch.css'
import {Button, Icon} from 'react-materialize'

class Switch extends React.Component{
    constructor(props){
        super(props)
        this.onClick = this.onClick.bind(this);
    }

    onClick(){
        this.props.toggle();
    }

    render(){
        return(
            <div className="switch">
                <label>{this.props.name}
                <input id="checkBox" type="checkbox" onChange={this.onClick} checked={this.props.power} ></input>
                </label>
            </div>
        )
    }
}

export default Switch;