import React from 'react';
import './slider.css';

class Slider extends React.Component{
    constructor(props){
        super(props)
        this.setValue = this.setValue.bind(this);
    }

    setValue(event){
        this.props.setValue(event.target.value);
    }

    render(){
        return(
            <div className="slider">
                <input type="range" onChange={this.setValue} disabled={!this.props.power} value={this.props.volume}></input>
            </div>
        )
    }
}

export default Slider;